;;
;; Chargement de emacs-init.org avec le version de org la plus récente disponible
;;
(if (file-exists-p (expand-file-name "straight/build/org/org.el" user-emacs-directory))
    (add-to-list 'load-path (expand-file-name "straight/build/org/" user-emacs-directory)))
(require 'org)

(org-babel-load-file
  (expand-file-name
   "emacs-config.org"
   user-emacs-directory))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-show-quick-access t nil nil "Customized with use-package company")
 '(org-agenda-files nil nil nil "Customized with use-package org")
 '(package-selected-packages '(rainbow-delimiters tuareg))
 '(safe-local-variable-values
   '((magit-todos-exclude-globs "nsswitch.aug" "system-config-printer-campux.py" "campux-gdm3.css" "ubuntu.css"))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
