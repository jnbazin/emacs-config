(setq package-enable-at-startup nil)

;;
;; Optimisation du temps de chargement
;; Inspiré de la FAQ de doom : https://github.com/hlissner/doom-emacs/blob/develop/docs/faq.org
;;
;; Sauvegarde de la valeur de file-name-handler-alist à restaurer
(defvar my/default-file-name-handler-alist file-name-handler-alist)

;; Définition des fonctions affectant les valeurs souhaitées
(defun my/set-max-gc-cons-threshold ()
  (setq gc-cons-threshold most-positive-fixnum)) ; 2^61 bytes
(defun my/set-default-gc-cons-threshold ()
  (run-at-time 1 nil ; délai de 1s pour profiter plus longtemps de l'absence de
                     ; déclenchement du GC
               (lambda () (setq gc-cons-threshold 16777216)))) ; 16 Mb

(defun my/erase-file-name-handler-alist ()
  (setq file-name-handler-alist nil))
(defun my/restore-file-name-handler-alist ()
  (setq file-name-handler-alist my/default-file-name-handler-alist))

;; Application de la configuration souhaitée
(my/set-max-gc-cons-threshold)
(my/erase-file-name-handler-alist)
(add-hook 'emacs-startup-hook 'my/set-default-gc-cons-threshold)
(add-hook 'emacs-startup-hook 'my/restore-file-name-handler-alist)
;;
;; Buffer *Messages* avec timestamp
;;
(defun my/ad-timestamp-message (FORMAT-STRING &rest args)
  "Advice to run before `message' that prepends a timestamp to each message.
Activate this advice with:
  (advice-add 'message :before 'my/ad-timestamp-message)
Deactivate this advice with:
  (advice-remove 'message 'my/ad-timestamp-message)"
  (if message-log-max
      (let ((deactivate-mark nil)
            (inhibit-read-only t))
        (with-current-buffer "*Messages*"
          (goto-char (point-max))
          (if (not (bolp))
              (newline))
          (insert (format-time-string "[%F %T.%3N] "))))))
(advice-add 'message :before 'my/ad-timestamp-message)


;;
;; Caractéristiques de la frame par défaut
;;
(setq default-frame-alist
      `((tool-bar-lines . 0)
        (menu-bar-lines . ,(if (display-graphic-p) 1 0))
        (width . 132)
        (height . 43)
        ))


;;
;; Désactivation des raccourcis associés à suspend-frame
;; qui deviennent donc disponibles pour d'autres usages
;;
(global-unset-key (kbd "C-z"))
(global-unset-key (kbd "C-x C-z"))

